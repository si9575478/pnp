import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

function Accordian() {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="holder.js/100px180?text=Image cap" />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
        The “PEOPLES NATIONAL PARTY” herein shall work against T errorism, anti-National & Fringe elements, Corruption, Nepotism and Favoritism which is corroding the basic fabric of our Nation.
The Party shall establish a research and policy institute that shall do the research work on ground and accordingly frame policies in collaboration with the state apparatus, the policy institute will have a separate constitution, norms and guidelines.
The Party PEOPLES NATIONAL PARTY (PNP)shall work to safeguard the integrity of union of India.

The Party PEOPLES NATIONAL PARTY (PNP)will strictly adhere to the aims and objectives/ bylaw to work for the public welfare.

An attempt to influence policy and public debate

It lists action to be taken in the following sectors: Society, Culture and Peace; Democracy; livelihoods & Employment; Economy and technology; food, water and energy; Health and Hygiene; Environment and Ecology; Settlements; Transportation; Learning, Education, and knowledge; Media; Law, Justice, and Custom; and Global relations.


The manifesto is in particular part of the fourth objective, as an attempt to influence policy and public debate on real issues affecting people. Unfortunately elections in India have degenerated into slanging matches, rather than opportunities for public dialogue on key issues. Also, the last few years have witnessed a major attack on some of what is most cherished in India, including the freedom of speech, the freedom to practice one‟s faith or ethics without fear, the co-existence between incredibly different cultures. Along with several other similar citizens‟ efforts, the manifesto‟s signatories hope that it can contribute in a small way to reversing these trends.
The manifesto begins by noting the multiple crises India faces, including ecological collapse, socio-economic inequities and continuing deprivation. It states:
“We commit, and ask all political parties, people‟s movements, civil society organisations, and other relevant groups and collectives to commit to an India that is just, equitable, and sustainable for today‟s and coming generations, where:

the well-being and health of all is ensured by providing opportunities to engage in materially, culturally, ethically and spiritually fulfilling lives and livelihoods;

everyone has meaningful avenues of directly participating in decision-making through direct forms of democracy;
 
there is no discrimination based on gender, caste, class, ethnicity, religion, „race‟, ability, sexual orientation, and other such features;

the diversity and pluralism of cultures and knowledge‟s and faiths is respected and enabled to co- exist harmoniously; and

there is respect for the rest of nature and the ecological conditions on which all life depends.”

        </Card.Text>
      </Card.Body>
     
    
    </Card>
  );
}

export default Accordian;