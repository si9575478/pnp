// import React, {Component} from 'react';
import React,{useState} from 'react';

import Axios from 'axios';
import Navbarr from '../components/Navbarr.js';
import Footer from '../components/Footer.js';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import './Forms.css';


function Register() {
    //For Insert
    const [name,setName]=useState("");
    const [email,setEmail]=useState("");
    const[password,setPassword]=useState('');
    const[cpassword,setCpassword]=useState('');
   
    
    const insert=()=>{
      //console.log(foodName + days);
      Axios.post("http://localhost:8000/register",{
        name:name,
        email:email,
        password:password,
        cpassword:cpassword
          
      
      });
    };
   

//
  return (
    <div className='forms'>
      <Navbarr/>
    <Form className='form'>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" value={name} onChange={(e)=>setName(e.target.value)} required/>
        <Form.Text className="text-muted">
         
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="text"  value={email} onChange={(e)=>setEmail(e.target.value)} required valid/>
        <Form.Text className="text-muted">
      
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Password</Form.Label>
        <Form.Control type="text"  value={password} onChange={(e)=>setPassword(e.target.value)} required/>
        <Form.Text className="text-muted">
       
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control type="text" value={cpassword}  onChange={(e)=>setCpassword(e.target.value)} required />
        <Form.Text className="text-muted">
         
        </Form.Text>
      </Form.Group>

    
      <br/><br/><br/>

    
      <Button variant="primary" className="mb-5" type="submit" onClick={insert}>
        Submit
      </Button>
     
    </Form>
  
    <Footer/>
    </div>
  )
}
    
    
  
export default Register;